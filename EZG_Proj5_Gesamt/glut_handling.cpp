
#include "glut_handling.h"

#include <GL/glut.h>

// for glutSetOption function
// for makros GLUT_ACTION_ON_WINDOW_CLOSE and GLUT_ACTION_GLUTMAINLOOP_RETURNS
#include <GL/freeglut.h>

#include "graphic.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <GL/glext.h>
#include "Extensions/ARB_multitexture_extension.h"
#include "Extensions/ARB_texture_cube_map_extension.h"
#include "Extensions/ARB_texture_env_combine_extension.h"
#include "Extensions/ARB_texture_env_dot3_extension.h"

#include <iostream>

#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 600

using namespace std;

GlutHandling* GlutHandling::sGh = NULL;

GlutHandling::GlutHandling()
	:mGraphic(NULL)
{
	sGh = this;
}

GlutHandling::~GlutHandling()
{
	delete mGraphic;
}

bool GlutHandling::init(int argc, char** argv)
{
	// GLUT Window Initialization:
	glutInit(&argc, argv);

	glutInitWindowPosition(0,0);
	glutInitWindowSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	//glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);

	glutCreateWindow("EZG GesamtProjekt Lukas Branigan");

	mGraphic = new Graphic(DEFAULT_WIDTH, DEFAULT_HEIGHT);

	//glEnable(GL_MULTISAMPLE);
	//glEnable(GL_MULTISAMPLE_ARB);
	//glEnable(GL_LINE_SMOOTH);
	//glEnable(GL_POLYGON_SMOOTH);


	if (!mGraphic->init()) {
		return false;
	}

	GLint buf, sbuf;
	glGetIntegerv(GL_SAMPLE_BUFFERS, &buf);
	cout << "number of sample buffers is " << buf << endl;
	glGetIntegerv(GL_SAMPLES, &sbuf);
	cout << "number of samples is " << sbuf << endl;

	// *** begin - set callback functions ***

	glutVisibilityFunc(visibilityFunc);
	glutReshapeFunc(reshapeFunc);
	glutDisplayFunc(displayFunc);
	glutIdleFunc(idleFunc);
	glutOverlayDisplayFunc(overlayDisplayFunc);
	glutCloseFunc(closeFunc);
	atexit(exitFunc);

	glutKeyboardFunc(keyboardPressFunc);
	glutKeyboardUpFunc(keyboardReleaseFunc);
	glutSpecialFunc(specialKeyboardPressFunc);
	glutSpecialUpFunc(specialKeyboardReleaseFunc);

	glutMouseFunc(mouseFunc);
	glutMotionFunc(activeMotionFunc);
	glutPassiveMotionFunc(passiveMotionFunc);

	// *** end - set callback functions ***

	return true;
}

void GlutHandling::run()
{
	glutMainLoop();
	cout << L"glutMainLoop finished" << endl;

	// another thread could close the main loop with glutLeaveMainLoop();
}

void GlutHandling::visibilityFunc(int state)
{
	sGh->visibility(state);
}

void GlutHandling::reshapeFunc(int width, int height)
{
	sGh->reshape(width, height);
}

void GlutHandling::displayFunc()
{
	sGh->display();
}

void GlutHandling::idleFunc()
{
	sGh->idle();
}

void GlutHandling::overlayDisplayFunc()
{
	sGh->overlayDisplay();
}

void GlutHandling::closeFunc()
{
	sGh->close();
}

void GlutHandling::exitFunc()
{
	sGh->exit();
}

void GlutHandling::keyboardPressFunc(unsigned char key, int x, int y)
{
	keyboardFunc(key, x, y, true);
}

void GlutHandling::keyboardReleaseFunc(unsigned char key, int x, int y)
{
	keyboardFunc(key, x, y, false);
}

void GlutHandling::specialKeyboardPressFunc(int key, int x, int y)
{
	keyboardFunc(key + 256, x, y, true);
}

void GlutHandling::specialKeyboardReleaseFunc(int key, int x, int y)
{
	keyboardFunc(key + 256, x, y, false);
}

void GlutHandling::keyboardFunc(int key, int x, int y, bool isPressed)
{
	sGh->keyboard(key, x, y, isPressed);
}

void GlutHandling::mouseFunc(int button, int state, int x, int y)
{
	sGh->mouse(button, state, x, y);
}

void GlutHandling::activeMotionFunc(int x, int y)
{
	motionFunc(x, y, false);
}

void GlutHandling::passiveMotionFunc(int x, int y)
{
	motionFunc(x, y, true);
}

void GlutHandling::motionFunc(int x, int y, bool isPassive)
{
	sGh->motion(x, y, isPassive);
}

void GlutHandling::visibility(int state)
{
	cout << "visibility: " << state << endl;
}

void GlutHandling::reshape(int width, int height)
{
	mGraphic->reshape(width, height);
	cout << "reshape: " << width << " " << height << endl;
}

void GlutHandling::display()
{
	mGraphic->update();
	glutSwapBuffers();
	//glutPostRedisplay();

	//cout << "display" << endl;
}

void GlutHandling::idle()
{
	mGraphic->update();
	glutSwapBuffers();
	//cout << "idle" << endl;
}

void GlutHandling::overlayDisplay()
{
	cout << "overlapDisplay" << endl;
}

void GlutHandling::close()
{
	cout << "close" << endl;
}

void GlutHandling::exit()
{
	cout << "exit" << endl;
}

void GlutHandling::keyboard(int key, int x, int y, bool isPressed)
{
	switch (key) {
		case 49: // 1
			if (isPressed) {
				mGraphic->renderdrawBump();
			}
			break;
		case 51: // 3
			if (isPressed) {
				mGraphic->renderKdTree();
			}
			break;
		case 52: // 4
			if (isPressed) {
				mGraphic->renderAntialiasing();
			}
			break;
		case 50: // 2
			if (isPressed) {
				mGraphic->renderwireframe();
			}
			break;
		case 53: // 5
			if (isPressed) {
				mGraphic->delPoints();
			}
			break;
		case 32: // space key
			if (isPressed) {
				mGraphic->setPoint();
			}
			break;
		case 13: // enter key
			mGraphic->getCamera()->moveForward(isPressed);
			break;
		case 8: // backward key
			mGraphic->getCamera()->moveBackward(isPressed);
			break;
		case 356: // left
			mGraphic->getCamera()->rotateLeft(isPressed);
			break;
		case 358: // right
			mGraphic->getCamera()->rotateRight(isPressed);
			break;
		case 357: // up
			mGraphic->getCamera()->rotateUp(isPressed);
			break;
		case 359: // down
			mGraphic->getCamera()->rotateDown(isPressed);
			break;
		case 360: // page up
			mGraphic->getCamera()->rotateAntiClockwise(isPressed);
			break;
		case 361: // page down
			mGraphic->getCamera()->rotateClockwise(isPressed);
			break;
		default:
			
			break;
	}
}

void GlutHandling::mouse(int button, int state, int x, int y)
{
	cout << "mouse: btn: " << button << " state: " << state <<
			" x: " << x << " y: " << y << endl;
}

void GlutHandling::motion(int x, int y, bool isPassive)
{
	//cout << "motion: " << x << " " << y << " " << isPassive << endl;
}
