
#ifndef __KD_LEAF_H__
#define __KD_LEAF_H__

#include "kd_node.h"
#include <vector>

class Triangle;

class KdLeaf : public KdNode
{
public:
	KdLeaf();
	virtual ~KdLeaf();
	std::vector<Triangle*> mTriangles;
};

#endif