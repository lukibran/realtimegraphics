//////////////////////////////////////////////////////////////////////////////////////////
//	Normalisation Cube Map.cpp
//	Generate normalisation cube map
//	Downloaded from: www.paulsprojects.net
//	Created:	20th July 2002
//
//	Copyright (c) 2006, Paul Baker
//	Distributed under the New BSD Licence. (See accompanying file License.txt or copy at
//	http://www.paulsprojects.net/NewBSDLicense.txt)
//////////////////////////////////////////////////////////////////////////////////////////	
#include "normalisation_cube_map.h"

#include <Windows.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <glm/gtc/quaternion.hpp>

static glm::vec3 getNormalizeAndPackedTo01(const glm::vec3& v)
{
	glm::vec3 temp = glm::normalize(v);

	temp = temp * 0.5f + glm::vec3(0.5f, 0.5f, 0.5f);

	return temp;
}


bool GenerateNormalisationCubeMap()
{
	unsigned char * data=new unsigned char[32*32*3];
	if(!data)
	{
		printf("Unable to allocate memory for texture data for cube map\n");
		return false;
	}

	//some useful variables
	int size=32;
	float offset=0.5f;
	float halfSize=16.0f;
	glm::vec3 tempVector;
	unsigned char * bytePtr;

	//positive x
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = halfSize;
			tempVector.y = -(j+offset-halfSize);
			tempVector.z = -(i+offset-halfSize);

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	//negative x
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = -halfSize;
			tempVector.y = -(j+offset-halfSize);
			tempVector.z = (i+offset-halfSize);

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	//positive y
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = i+offset-halfSize;
			tempVector.y = halfSize;
			tempVector.z = (j+offset-halfSize);

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	//negative y
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = i+offset-halfSize;
			tempVector.y = -halfSize;
			tempVector.z = -(j+offset-halfSize);

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	//positive z
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = i+offset-halfSize;
			tempVector.y = -(j+offset-halfSize);
			tempVector.z = halfSize;

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	//negative z
	bytePtr=data;

	for(int j=0; j<size; j++)
	{
		for(int i=0; i<size; i++)
		{
			tempVector.x = -(i+offset-halfSize);
			tempVector.y = -(j+offset-halfSize);
			tempVector.z = -halfSize;

			tempVector = getNormalizeAndPackedTo01(tempVector);

			bytePtr[0]=(unsigned char)(tempVector.x * 255.0f);
			bytePtr[1]=(unsigned char)(tempVector.y * 255.0f);
			bytePtr[2]=(unsigned char)(tempVector.z * 255.0f);

			bytePtr+=3;
		}
	}
	glTexImage2D(	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,
					0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	delete [] data;

	return true;
}
