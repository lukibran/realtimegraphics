
#ifndef __VECTOR_H__
#define __VECTOR_H__

#include "point.h"

class Vector: public Point
{
public:
	Vector(float x, float y, float z);
	virtual ~Vector();
};

#endif