
#include "graphic.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <GL/glut.h>

#include <iostream>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <GL/glext.h>
#include "Extensions/ARB_multitexture_extension.h"
#include "Extensions/ARB_texture_cube_map_extension.h"
#include "Extensions/ARB_texture_env_combine_extension.h"
#include "Extensions/ARB_texture_env_dot3_extension.h"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string.h>
#include "image.h"
#include "normalisation_cube_map.h"
#include <sstream>
#include <iomanip>

#include <algorithm>
#include "kd_leaf.h"
#include "vector.h"

#include "point.h"

using namespace std;

#include <time.h>
#include <windows.h> //I've ommited this line.
#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

#if 1
struct timezone 
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};
 
int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  unsigned __int64 tmpres = 0;
  static int tzflag;
 
  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);
 
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;
 
    /*converting file time to unix epoch*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS; 
    tmpres /= 10;  /*convert into microseconds*/
    tv->tv_sec = (long)(tmpres / 1000000UL);
    tv->tv_usec = (long)(tmpres % 1000000UL);
  }
 
  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }
    tz->tz_minuteswest = _timezone / 60;
    tz->tz_dsttime = _daylight;
  }
 
  return 0;
}

#endif

static void printMat4(const glm::mat4& m) {
	for (int i = 0; i < 16; i++) {
		if (!(i % 4)) {
			cout << endl;
		}
		cout << " " << (&m[0][0])[i];
	}
	cout << endl;
}

Graphic::Graphic(unsigned int width, unsigned int height)
	:w(width), h(height),
	mStartTime(getTime()),
	mCam(),
	mShadowMapSize(600),
	mShadowMapTexture(0),
	mLightProjectionMatrix(), mLightViewMatrix(),
	mCameraProjectionMatrix(), mCameraViewMatrix(),
	mLightPos(3.0f, 12.0f, 2.0f),
	//mLightCenter(-5.0f, 0.0f, 5.0f),
	mLightUp(Camera::getUpVector(glm::normalize(mLightCenter - mLightPos))),
	//mLightUp(0.0f, 1.0f, 0.0f),
	mTriangles(),
	mTorus(0.0f, 0.0f, 0.0f, 0.5f, 4.0f, 48, &mTriangles),
	mTorus2(0.0f, 2.0f, 0.0f, 1.0f, 1.0f, 48, &mTriangles),
	mDrawBumps(true),
	mDrawColor(true),
	mWireframe(false),
	mDrawKdTree(false),
	mDrawMode(0),
	mAntialiasing(false),
	mKdRoot(NULL),
	mReticleIsSet(false),
	mReticle(0.0f, 0.0f, 0.0f),
	point1Set(false),
	point1(0.0f, 0.0f, 0.0f),
	point2Set(false),
	point2(0.0f, 0.0f, 0.0f),
	mFloor(0.0f, -5.0f, 0.0f, 20.0f, 0.1f, 30.0f, &mTriangles)
{
	mCube[0] = new Cube(-13.0f, 0.0f, -3.0f, 1.0f, 1.0f, 1.0f, &mTriangles);
	mCube[1] = new Cube(-9.0f, 4.0f, 7.0f, 2.0f, 1.0f, 1.0f, &mTriangles);
	mCube[2] = new Cube(-7.0f, -3.0f, -10.0f, 1.5f, 1.5f, 1.5f, &mTriangles);
	mCube[3] = new Cube(-13.0f, -3.0f, 5.0f, 1.5f, 1.5f, 1.5f, &mTriangles);
	mCube[4] = new Cube(-6.0f, 4.0f, -5.0f, 1.5f, 1.5f, 1.5f, &mTriangles);
	mCube[5] = new Cube(-8.0f, 8.0f, 9.0f, 1.5f, 1.5f, 1.5f, &mTriangles);

	//addFloor(glm::vec3(0.0f, -2.0f, 0.0f), 50.0f);

	buildKdTree();
}

Graphic::~Graphic()
{
	for (unsigned int i = 0; i < CUBE_COUNT; i++) {
		delete mCube[i];
	}
}

bool Graphic::init()
{
	cout << "version: " << glGetString(GL_VERSION) << endl;
	//Check for and set up extensions
	if(	!SetUpARB_multitexture()		||	!SetUpARB_texture_cube_map()	||
		!SetUpARB_texture_env_combine()	||	!SetUpARB_texture_env_dot3())
	{
		printf("Required Extension Unsupported\n");
		exit(0);
	}

	//Load identity modelview
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Shading states
	glShadeModel(GL_SMOOTH);
	glClearColor(1.2f, 0.4f, 0.2f, 0.0f); // specify clear values for the color buffers
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // specify implementation-specific hints

	// Depth states
	glClearDepth(1.0f); // specify the clear value for the depth buffer
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);

	// Blend
    glEnable(GL_BLEND); //Enable alpha blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set the blend function

	enableDisableCullface();

	loadMaps();
	mCubeNormalMap[0] = mNormalMap;
	mCubeDecalTexture[0] = mDecalTexture;

	loadMaps(mCubeNormalMap[0], mCubeDecalTexture[0],
			"normal_map2.bmp", "brick.bmp");
	loadMaps(mCubeNormalMap[1], mCubeDecalTexture[1],
			"normal_map2.bmp", "brick.bmp");
	loadMaps(mCubeNormalMap[2], mCubeDecalTexture[2],
			"normal_map2.bmp", "brick.bmp");
	loadMaps(mCubeNormalMap[3], mCubeDecalTexture[3],
			"normal_map2.bmp", "brick.bmp");
	loadMaps(mCubeNormalMap[4], mCubeDecalTexture[4],
			"normal_map2.bmp", "brick.bmp");
	loadMaps(mCubeNormalMap[5], mCubeDecalTexture[5],
			"normal_map2.bmp", "brick.bmp");

	// We use glScale when drawing the scene
	glEnable(GL_NORMALIZE);

	//Create the shadow map texture
	glGenTextures(1, &mShadowMapTexture);
	glBindTexture(GL_TEXTURE_2D, mShadowMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, mShadowMapSize, mShadowMapSize, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	//Use the color as the ambient and diffuse material
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	//White specular material color, shininess 16
	float col[] = {1.0f, 1.0f, 1.0f, 1.0f};
	glMaterialfv(GL_FRONT, GL_SPECULAR, col);
	glMaterialf(GL_FRONT, GL_SHININESS, 16.0f);

	//Calculate & save matrices
	glPushMatrix();

	glLoadIdentity();
	gluPerspective(mCam.getViewAngle(), (GLfloat) w / (GLfloat) h,
			mCam.getZNear(), mCam.getZFar()); // set up a perspective projection matrix
	glGetFloatv(GL_MODELVIEW_MATRIX, &mCameraProjectionMatrix[0][0]);

	glLoadIdentity();
	{
		glm::vec3 eye, center, up;
		mCam.getPropertiesForGluLookAt(eye, center, up);
		gluLookAt(eye.x, eye.y, eye.z, // eye x, y, z
				center.x, center.y, center.z, // center x, y, z
				up.x, up.y, up.z); // up vector x, y, z
	}
	glGetFloatv(GL_MODELVIEW_MATRIX, &mCameraViewMatrix[0][0]);

	glLoadIdentity();
	gluPerspective(70.0f, 1.0f, 3.0f, 30.0f); // angle must be big enough for the full scene
	glGetFloatv(GL_MODELVIEW_MATRIX, &mLightProjectionMatrix[0][0]);

	glLoadIdentity();
	gluLookAt(mLightPos.x, mLightPos.y, mLightPos.z,
			mLightCenter.x, mLightCenter.y, mLightCenter.z,
			mLightUp.x, mLightUp.y, mLightUp.z);
	glGetFloatv(GL_MODELVIEW_MATRIX, &mLightViewMatrix[0][0]);

	glPopMatrix();

	return true;
}

void Graphic::reshape(unsigned int width, unsigned int height)
{
	if (height == 0) {
		height = 1;
	}

	w = width;
	h = height;

	//Update the camera's projection matrix
	glPushMatrix();
	glLoadIdentity();
	gluPerspective(mCam.getViewAngle(), (GLfloat) w / (GLfloat) h,
			mCam.getZNear(), mCam.getZFar()); // set up a perspective projection matrix
	glGetFloatv(GL_MODELVIEW_MATRIX, &mCameraProjectionMatrix[0][0]);
	glPopMatrix();
}

void Graphic::update()
{
	// update camera position

	int64_t currentTime = getTime();
	double sec = double(currentTime - mStartTime) / 1000000.0;

	mCam.update(sec);

	mTorus.calcTanSpaceLightVector(mLightPos);
	mTorus2.calcTanSpaceLightVector(mLightPos);
	calcTanSpaceLightVectorForCubes(mLightPos);


	// ===== 1st pass - from light's point of view
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear Screen and Depth Buffer

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&mLightProjectionMatrix[0][0]);

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(&mLightViewMatrix[0][0]);

	//Use viewport the same size as the shadow map
	glViewport(0, 0, mShadowMapSize, mShadowMapSize);

	//Draw back faces into the shadow map
	glCullFace(GL_FRONT);

	//Disable color writes, and use flat shading for speed
	glShadeModel(GL_FLAT);
	glColorMask(0, 0, 0, 0);

	// Draw the scene
	drawForPass(1);

	// Read the depth buffer into the shadow map texture
	glBindTexture(GL_TEXTURE_2D, mShadowMapTexture);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, mShadowMapSize, mShadowMapSize);

	// restore states
	glCullFace(GL_BACK);
	glShadeModel(GL_SMOOTH);
	glColorMask(1, 1, 1, 1);

	// ===== 2nd pass - Draw from camera's point of view
	glClear(GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&mCameraProjectionMatrix[0][0]);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	{
		glm::vec3 eye, center, up;
		mCam.getPropertiesForGluLookAt(eye, center, up);
		gluLookAt(eye.x, eye.y, eye.z, // eye x, y, z
				center.x, center.y, center.z, // center x, y, z
				up.x, up.y, up.z); // up vector x, y, z
	}
	glGetFloatv(GL_MODELVIEW_MATRIX, &mCameraViewMatrix[0][0]);

	glViewport(0, 0, w, h);

	//Use dim light to represent shadowed areas
	{
		glm::vec4 lp(mLightPos.x, mLightPos.y, mLightPos.z, 1.0f);
		glLightfv(GL_LIGHT1, GL_POSITION, &lp[0]);
	}
	float col[] = {0.2f, 0.2f, 0.2f, 0.2f};
	glLightfv(GL_LIGHT1, GL_AMBIENT, col);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, col);
	float black[] = {0.0f, 0.0f, 0.0f, 0.0f};
	glLightfv(GL_LIGHT1, GL_SPECULAR, black);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHTING);

	drawForPass(2);


	// ===== 3rd pass - Draw with bright light
	float white[] = {1.0f, 1.0f, 1.0f, 1.0f};
	glLightfv(GL_LIGHT1, GL_DIFFUSE, white);
	glLightfv(GL_LIGHT1, GL_SPECULAR, white);

	//Calculate texture matrix for projection
	//This matrix takes us from eye space to the light's clip space
	//It is postmultiplied by the inverse of the current view matrix when specifying texgen
	float bias = 0.5006f;
	static glm::mat4 biasMatrix(
			bias, 0.0f, 0.0f, 0.0f,
			0.0f, bias, 0.0f, 0.0f,
			0.0f, 0.0f, bias, 0.0f,
			bias, bias, bias, 1.0f);   //bias from [-1, 1] to [0, 1]
	glm::mat4 textureMatrix = biasMatrix * mLightProjectionMatrix * mLightViewMatrix;

	//Set up texture coordinate generation.
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_S, GL_EYE_PLANE, &getRowFromMat4(textureMatrix, 0)[0]);
	glEnable(GL_TEXTURE_GEN_S);

	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_T, GL_EYE_PLANE, &getRowFromMat4(textureMatrix, 1)[0]);
	glEnable(GL_TEXTURE_GEN_T);

	glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_R, GL_EYE_PLANE, &getRowFromMat4(textureMatrix, 2)[0]);
	glEnable(GL_TEXTURE_GEN_R);

	glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
	glTexGenfv(GL_Q, GL_EYE_PLANE, &getRowFromMat4(textureMatrix, 3)[0]);
	glEnable(GL_TEXTURE_GEN_Q);

	//Bind & enable shadow map texture
	glBindTexture(GL_TEXTURE_2D, mShadowMapTexture);
	glEnable(GL_TEXTURE_2D);

	//Enable shadow comparison
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE);

	//Shadow comparison should be true (ie not in shadow) if r<=texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);

	//Shadow comparison should generate an INTENSITY result
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY);

	//Set alpha test to discard false comparisons
	//glAlphaFunc(GL_GEQUAL, 0.99f);
	glAlphaFunc(GL_GEQUAL, 0.49f);
	glEnable(GL_ALPHA_TEST);

	drawForPass(3);

	//Disable textures and texgen
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glDisable(GL_TEXTURE_GEN_R);
	glDisable(GL_TEXTURE_GEN_Q);

	//Restore other states
	glDisable(GL_LIGHTING);
	glDisable(GL_ALPHA_TEST);

	//reset matrices
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glFinish();
}

void Graphic::renderdrawBump()
{
	mDrawBumps = !mDrawBumps;
}

void Graphic::renderwireframe()
{
	mWireframe = !mWireframe;
	if (mWireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		enableDisableCullface();
	}
	else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		enableDisableCullface();
	}
}

void Graphic::renderKdTree()
{
	mDrawKdTree = !mDrawKdTree;
}

void Graphic::renderdrawMode()
{
	mDrawMode++;
	if (mDrawMode >= 3) {
		mDrawMode = 0;
	}
	enableDisableCullface();
}

void Graphic::renderAntialiasing()
{
	mAntialiasing = !mAntialiasing;
	if (mAntialiasing) {
		glEnable(GL_MULTISAMPLE);
	}
	else {
		glDisable(GL_MULTISAMPLE);
	}
	cout << "antialiasing " << mAntialiasing << endl;
}

void Graphic::delPoints()
{
	point1Set = false;
	point2Set = false;
}

void Graphic::setPoint()
{
	glm::vec3 p = mCam.getPos();
	glm::vec3 v = mCam.getViewVector();

	float d = 0.0f;
	Triangle* t = visitNodes(mKdRoot, Point(p.x, p.y, p.z),
			Vector(v.x, v.y, v.z), 100.0f, d);

	if (t) {
		mReticleIsSet = true;
		mReticle = p + glm::normalize(v) * d;
	}
	else {
		mReticleIsSet = false;
	}


	if (mReticleIsSet) {
		if (point2Set) {
			point1Set = true;
			point1 = point2;
			// no return --> set p2 with reticle
		}
		if (point1Set) {
			point2Set = true;
			point2 = mReticle;
		}
		else {
			point1Set = true;
			point1 = mReticle;
		}
	}
	printInfo();
}

void Graphic::enableDisableCullface()
{
	if (mWireframe || mDrawMode > 0) {
		glDisable(GL_CULL_FACE);
	}
	else {
		glEnable(GL_CULL_FACE);
	}
}

// Schadowmapping Durchg�nge f�rs zeichnen der Objekte
void Graphic::drawForPass(unsigned int passNr)
{

	// Zeichnen der Linie zwischen den zwei erzeugten Punkten
	if (point1Set && point2Set && passNr >= 2) {
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		glLineWidth(2.0);
		glBegin(GL_LINES);
		glVertex3f(point1.x, point1.y, point1.z);
		glVertex3f(point2.x, point2.y, point2.z);
		glEnd();
		glLineWidth(1.0);
	}

	float col[] = {0.0f, 0.0f, 1.0f, 1.0f};
	switch (mDrawMode)
	{
		case 0: 
			switch (passNr) {
				case 1: // Rendern der Objekte aus Sicht des Lichts
					mFloor.drawSimpleColor(col);
					drawTorus(true, true, true);
					drawCubes(true, true, true);
					break;
				case 2: //  Rendern der Objekte aus der Cameraposition
					mFloor.drawSimpleColor(col);
					drawTorus(true, true, true);
					drawCubes(true, true, true);
					break;
				case 3: // Zeichnen mit hellem Licht 
					mFloor.drawSimpleColor(col);
					finalDrawCubes();

					glBlendFunc(GL_DST_COLOR, GL_SRC_ALPHA);
					glColor4f(1.0, 1.0, 1.0, 0.5);
					mTorus.drawColor(mDecalTexture, false);
					mTorus2.drawColor(mDecalTexture, false);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set the blend function
					break;
			}
			break;
		case 1: // triangles
			glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
			if (!mWireframe) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glEnable(GL_CULL_FACE);
			}
			drawTriangles();
			if (!mWireframe) {
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				enableDisableCullface();
			}
			break;
		case 2: // none
			break;
	}

	if (passNr >= 2) {
		if (mDrawKdTree) {
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			drawKdTree();
		}
		
		glPointSize(10.0f);

		// Punkt in der Mitte des Bildschirms zeichnen
		glm::vec3 p = mCam.getPos() + mCam.getViewVector() * (mCam.getZNear() * 1.1f);
		glBegin(GL_POINTS);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glVertex3f(p.x, p.y, p.z);

		// Die 2 ausgew�hlten Punkte zeichnen
		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		if (point1Set) {
			glVertex3f(point1.x, point1.y, point1.z);
		}
		if (point2Set) {
			glVertex3f(point2.x, point2.y, point2.z);
		}
		glEnd();

		glPointSize(1.0f);
	}
}

void Graphic::drawTorus(bool drawBumps, bool drawColor, bool setColor)
{
	if (!mDrawBumps || mWireframe) {
		drawBumps = false;
	}
	if (!mDrawColor) {
		drawColor = false;
	}
	
	if (drawBumps) {
		if (setColor) {
			glColor4f(1.0, 1.0, 1.0, 1.0); 
		}
		mTorus.drawBumpPass(mNormalMap, mNormalisationCubeMap);
		mTorus2.drawBumpPass(mNormalMap, mNormalisationCubeMap);
	}
	if (drawBumps && drawColor) {
		//Enable multiplicative blending
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
		//glEnable(GL_BLEND);
	}
	if (drawColor) {
		if (setColor) {
			glColor4f(1.0, 1.0, 1.0, 1.0); 
		}
		mTorus.drawColor(mDecalTexture, true);
		mTorus2.drawColor(mDecalTexture, true);

		//Disable texture
		glDisable(GL_TEXTURE_2D);

		//disable vertex arrays
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	if (drawBumps && drawColor){
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	}
}

void Graphic::printInfo()
{
	glm::vec3 pos;
	pos = mCam.getPos();

	cout << "Koordinaten der KameraPosition:" << pos.x << "," << pos.y <<  "," << pos.z  << endl;
	cout << "Koordinaten von Punkt 1:" << point1.x << "," << point1.y << "," << point1.z << endl;
	cout << "Koordinaten von Punkt 2:" << point2.x << "," << point2.y << "," << point2.z << endl;

	glm::vec3 diff1(pos-mReticle);
	cout << "Abstand von Kamera zu Punkt:" << glm::length(diff1) << endl; 
	glm::vec3 diff2(point1-point2);
	cout << "Abstand von Punkt 1 zu Punkt 2:" << glm::length(diff2) << endl << endl;

}

int64_t Graphic::getTime() const
{
	timeval tv;
	if(gettimeofday(&tv, NULL) == -1) {
		cerr << "gettimeofday failed" << endl;
		return -1;
	}
	return int64_t(tv.tv_sec) * int64_t(1000000) + int64_t(tv.tv_usec);
}

void Graphic::setCamera()
{
	{
		glm::vec3 eye, center, up;
		mCam.getPropertiesForGluLookAt(eye, center, up);
		gluLookAt(eye.x, eye.y, eye.z, // eye x, y, z
				center.x, center.y, center.z, // center x, y, z
				up.x, up.y, up.z); // up vector x, y, z
	}
}

glm::vec4 Graphic::getRowFromMat4(const glm::mat4& m, unsigned int rowIndex)
{
	return glm::vec4(m[0][rowIndex], m[1][rowIndex], m[2][rowIndex], m[3][rowIndex]);
}

void Graphic::loadMaps()
{
	//Load normal map
	Image normalMapImage;
	normalMapImage.Load("normal_map.bmp");
	normalMapImage.ExpandPalette();

	//Convert normal map to texture
	glGenTextures(1, &mNormalMap);
	glBindTexture(GL_TEXTURE_2D, mNormalMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, normalMapImage.width, normalMapImage.height,
					0, normalMapImage.format, GL_UNSIGNED_BYTE, normalMapImage.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Load decal image
	Image decalImage;
	decalImage.Load("gr�n.bmp");
	decalImage.ExpandPalette();

	//Convert decal image to texture
	glGenTextures(1, &mDecalTexture);
	glBindTexture(GL_TEXTURE_2D, mDecalTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, decalImage.width, decalImage.height,
					0, decalImage.format, GL_UNSIGNED_BYTE, decalImage.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


	//Create normalisation cube map
	glGenTextures(1, &mNormalisationCubeMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, mNormalisationCubeMap);
	GenerateNormalisationCubeMap();
	glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void Graphic::loadMaps(GLuint& normalMap, GLuint& decalTexture,
		const std::string& normalFile, const std::string& decalFile)
{
	//Load normal map
	Image normalMapImage;
	normalMapImage.Load(normalFile.c_str());
	normalMapImage.ExpandPalette();

	//Convert normal map to texture
	glGenTextures(1, &normalMap);
	glBindTexture(GL_TEXTURE_2D, normalMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, normalMapImage.width, normalMapImage.height,
					0, normalMapImage.format, GL_UNSIGNED_BYTE, normalMapImage.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Load decal image
	Image decalImage;
	decalImage.Load(decalFile.c_str());
	decalImage.ExpandPalette();

	//Convert decal image to texture
	glGenTextures(1, &decalTexture);
	glBindTexture(GL_TEXTURE_2D, decalTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, decalImage.width, decalImage.height,
					0, decalImage.format, GL_UNSIGNED_BYTE, decalImage.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Graphic::calcTanSpaceLightVectorForCubes(const glm::vec3& objectLightPosition)
{
	for (unsigned int i = 0; i < CUBE_COUNT; i++) {
		mCube[i]->calcTanSpaceLightVector(objectLightPosition);
	}
}

void Graphic::drawCubes(bool drawBumps, bool drawColor, bool setColor)
{
	for (unsigned int i = 0; i < CUBE_COUNT; i++) {
		drawCube(drawBumps, drawColor, setColor, *mCube[i], i);
	}
}

void Graphic::drawCube(bool drawBumps, bool drawColor, bool setColor, Cube& cube, unsigned int index)
{
	if (!mDrawBumps || mWireframe) {
		drawBumps = false;
	}
	if (!mDrawColor) {
		drawColor = false;
	}

	if (drawBumps) {
		if (setColor) {
			glColor4f(1.0, 1.0, 1.0, 1.0); // reset gl color, very important!!!
		}
		cube.drawBumpPass(mCubeNormalMap[index], mNormalisationCubeMap);
	}
	if (drawBumps && drawColor) {
		//Enable multiplicative blending
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
		//glEnable(GL_BLEND);
	}
	if (drawColor) {
		if (setColor) {
			glColor4f(1.0, 1.0, 1.0, 1.0); // reset gl color, very important!!!
		}
		cube.drawColor(mCubeDecalTexture[index], true);

		//Disable texture
		glDisable(GL_TEXTURE_2D);

		//disable vertex arrays
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	if (drawBumps && drawColor) {
	    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set the blend function
	}
}

void Graphic::finalDrawCubes()
{
	glBlendFunc(GL_DST_COLOR, GL_SRC_ALPHA);
	glColor4f(1.0, 1.0, 1.0, 0.5);
	for (unsigned int i = 0; i < CUBE_COUNT; i++) {
		mCube[i]->drawColor(mDecalTexture, false);
	}
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set the blend function
}

void Graphic::buildKdTree()
{
	std::vector<Triangle*> triangles;
	for (unsigned int i = 0; i < mTriangles.size(); i++) {
		triangles.push_back(&mTriangles[i]);
	}

	if (!buildKdNode(&mKdRoot, triangles)) {
		cout << "ERROR!!!! Build Kd-Tree failed!!!!!!" << endl;
		return;
	}

	if (!insertTrianglesIntoKdNode(mKdRoot, triangles)) {
		cout << "ERROR!!!! insert triangles to Kd-Tree failed!!!!!!" << endl;
		return;
	}

	cout << "Build successful Kd-Tree" << endl;
}

bool Graphic::buildKdNode(KdNode** parrent, const std::vector<Triangle*>& triangles)
{
	unsigned int length = triangles.size();
	std::vector<float> x(length, 0.0f), y(length, 0.0f), z(length, 0.0f);
	for (unsigned int i = 0; i < length; i++) {
		x[i] = triangles[i]->mCenter.mX;
		y[i] = triangles[i]->mCenter.mY;
		z[i] = triangles[i]->mCenter.mZ;
	}

	auto minmaxX = minmax_element(x.begin(), x.end());
	float lengthX = *minmaxX.second - *minmaxX.first;

	auto minmaxY = minmax_element(y.begin(), y.end());
	float lengthY = *minmaxY.second - *minmaxY.first;

	auto minmaxZ = minmax_element(z.begin(), z.end());
	float lengthZ = *minmaxZ.second - *minmaxZ.first;

	bool rv = false;
	// find the biggest length (x, y or z)
	if (lengthX >= lengthY && lengthX >= lengthZ) {
		// x
		rv = kdSplit(parrent, triangles, x, KdNode::DIR_X);
	}
	else {
		if (lengthY >= lengthZ) {
			// y
			rv = kdSplit(parrent, triangles, y, KdNode::DIR_Y);
		}
		else {
			// z
			rv = kdSplit(parrent, triangles, z, KdNode::DIR_Z);
		}
	}
	return rv;
}

bool Graphic::kdSplit(KdNode** parrent,
		const std::vector<Triangle*>& triangles,
		std::vector<float>& v,
		KdNode::Direction dir)
{
	if (v.empty()) {
		cout << "kdSplit: zero !!!!!!!!!!!!!" << endl;
		return false;
	}
	if (v.size() != triangles.size()) {
		cout << "kdSplit: wrong vector size" << endl;
		return false;
	}
	nth_element(v.begin(), v.begin() + v.size() / 2, v.end());
	float median = v[v.size() / 2];
	*parrent = new KdNode(dir, median);
	std::vector<Triangle*> trianglesLess;
	std::vector<Triangle*> trianglesMore;
	bool findMedian = false;
	for (unsigned int i = 0; i < triangles.size(); i++) {
		if (triangles[i]->mCenter.getValue(dir) < median) {
			trianglesLess.push_back(triangles[i]);
		}
		else {
			if (!findMedian && triangles[i]->mCenter.getValue(dir) <= median) {
				findMedian = true;
			}
			else {
				trianglesMore.push_back(triangles[i]);
			}
		}
	}
	if (trianglesLess.size() + trianglesMore.size() + 1 != triangles.size()) {
		cout << "FATAL ERROR: split size error !!!!!!" << endl;
		return false;
	}

	bool rv = true;
	if (trianglesLess.empty()) {
		(*parrent)->mLess = new KdLeaf();
	}
	else {
		rv = buildKdNode(&(*parrent)->mLess, trianglesLess);
	}

	if (trianglesMore.empty()) {
		(*parrent)->mMore = new KdLeaf();
	}
	else {
		rv = buildKdNode(&(*parrent)->mMore, trianglesMore);
	}
	return rv;
}

bool Graphic::insertTrianglesIntoKdNode(KdNode* n,
		const std::vector<Triangle*>& triangles)
{
	if (!n) {
		cout << "error: null node pointer" << endl;
		return false;
	}
	if (n->mDir == KdNode::DIR_NONE) {
		KdLeaf* leaf = dynamic_cast<KdLeaf*>(n);
		if (!leaf) {
			cout << "error: cast to KdLeaf failed (no leaf?)" << endl;
			return false;
		}
		leaf->mTriangles = triangles; // copy vector with all pointers
		return true;
	}
	std::vector<Triangle*> trianglesLess;
	std::vector<Triangle*> trianglesMore;
	for (unsigned int i = 0; i < triangles.size(); i++) {
		bool pushed = false;
		float epsilon = 0.001f;
		if (triangles[i]->mA.getValue(n->mDir) <= n->mBorder + epsilon ||
				triangles[i]->mB.getValue(n->mDir) <= n->mBorder + epsilon ||
				triangles[i]->mC.getValue(n->mDir) <= n->mBorder + epsilon) {
			trianglesLess.push_back(triangles[i]);
			pushed = true;
		}
		if (triangles[i]->mA.getValue(n->mDir) >= n->mBorder - epsilon ||
				triangles[i]->mB.getValue(n->mDir) >= n->mBorder - epsilon ||
				triangles[i]->mC.getValue(n->mDir) >= n->mBorder - epsilon) {
			trianglesMore.push_back(triangles[i]);
			pushed = true;
		}
		if (!pushed) {
			cout << "error: find no cell for triangle!!!" << endl;
		}
	}
	bool rv = true;
	if (!insertTrianglesIntoKdNode(n->mLess, trianglesLess)) {
		rv = false;
	}
	if (!insertTrianglesIntoKdNode(n->mMore, trianglesMore)) {
		rv = false;
	}
	return rv;
}

void Graphic::drawKdTree()
{
	drawKdTree(Point(-50, -50, -50), Point(50, 50, 50), mKdRoot);
}

void Graphic::drawTriangles()
{
	glBegin(GL_TRIANGLES);
	for (unsigned int i = 0; i < mTriangles.size(); i++) {
		glVertex3f(mTriangles[i].mA.mX, mTriangles[i].mA.mY, mTriangles[i].mA.mZ);
		glVertex3f(mTriangles[i].mB.mX, mTriangles[i].mB.mY, mTriangles[i].mB.mZ);
		glVertex3f(mTriangles[i].mC.mX, mTriangles[i].mC.mY, mTriangles[i].mC.mZ);
	}
	glEnd();
}

void Graphic::drawKdTree(const Point& minP, const Point& maxP, KdNode* n)
{
	if (!n || n->mDir == KdNode::DIR_NONE) {
		return;
	}
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBegin(GL_LINE_LOOP);
	switch (n->mDir) {
		case KdNode::DIR_X:
			glVertex3d(n->mBorder, minP.mY, minP.mZ);
			glVertex3d(n->mBorder, minP.mY, maxP.mZ);
			glVertex3d(n->mBorder, maxP.mY, maxP.mZ);
			glVertex3d(n->mBorder, maxP.mY, minP.mZ);
			break;
		case KdNode::DIR_Y:
			glVertex3d(minP.mX, n->mBorder, minP.mZ);
			glVertex3d(minP.mX, n->mBorder, maxP.mZ);
			glVertex3d(maxP.mX, n->mBorder, maxP.mZ);
			glVertex3d(maxP.mX, n->mBorder, minP.mZ);
			break;
		case KdNode::DIR_Z:
			glVertex3d(minP.mX, minP.mY, n->mBorder);
			glVertex3d(minP.mX, maxP.mY, n->mBorder);
			glVertex3d(maxP.mX, maxP.mY, n->mBorder);
			glVertex3d(maxP.mX, minP.mY, n->mBorder);
			break;
		default:
			break;
	}
	glEnd();

	switch (n->mDir) {
		case KdNode::DIR_X:
			drawKdTree(minP, Point(n->mBorder, maxP.mY, maxP.mZ), n->mLess);
			drawKdTree(Point(n->mBorder, minP.mY, minP.mZ), maxP, n->mMore);
			break;
		case KdNode::DIR_Y:
			drawKdTree(minP, Point(maxP.mX, n->mBorder, maxP.mZ), n->mLess);
			drawKdTree(Point(minP.mX, n->mBorder, minP.mZ), maxP, n->mMore);
			break;
		case KdNode::DIR_Z:
			drawKdTree(minP, Point(maxP.mX, maxP.mY, n->mBorder), n->mLess);
			drawKdTree(Point(minP.mX, minP.mY, n->mBorder), maxP, n->mMore);
			break;
		default:
			break;
	}
}

Triangle* Graphic::visitNodes(KdNode *n, const Point& a, const Vector& d, float tmax, float& distance)
{
	if (!n) {
		return NULL;
	}
	if (n->mDir == KdNode::DIR_NONE) {
		// leaf
		KdLeaf* leaf = dynamic_cast<KdLeaf*>(n);
		if (!leaf) {
			cout << "error: cast to KdLeaf failed" << endl;
			return NULL;
		}
		Triangle* minTriangle = NULL;
		//glBegin(GL_TRIANGLES);
		for (unsigned int i = 0; i < leaf->mTriangles.size(); i++) {
			float u, v, t;
			if (leaf->mTriangles[i]->rayTriangleIntersection(a.getVec3(),
					d.getVec3(), u, v, t) && t >= 0) {
				if (!minTriangle || t < distance) {
					minTriangle = leaf->mTriangles[i];
					distance = t;
				}
			}
		}
		//glEnd();
		return minTriangle;
	}

	// visit current node ...
	// which child to recurse first into? (0=near,1=far)
	bool first = (a.getValue(n->mDir) > n->mBorder) ? true : false;
	Triangle* minTriangle = NULL;
	if (d.getValue(n->mDir) == 0.0f) {
		// line segment parallel to splitting plane, visit near side only
		minTriangle = visitNodes(first ? n->mMore : n->mLess, a, d, tmax, distance);
	}
	else {
		//find t value for intersection
		float t = (n->mBorder - a.getValue(n->mDir)) / d.getValue(n->mDir);
		if (0.0f <= t && t < tmax) {
			minTriangle = visitNodes(first ? n->mMore : n->mLess, a, d, t, distance);

			Triangle* tmpTri = NULL;
			float tmpDist = 0.0f;
			tmpTri = visitNodes(!first ? n->mMore : n->mLess, a+d*t, d, tmax-t, tmpDist);
			if (tmpTri) {
				tmpDist = tmpDist + (d*t).getLength();
				if (!minTriangle || tmpDist < distance) {
					distance = tmpDist;
					minTriangle = tmpTri;
				}
			}
		}
		else {
			minTriangle = visitNodes(first ? n->mMore : n->mLess, a, d, tmax, distance);
		}
	}
	return minTriangle;
}
