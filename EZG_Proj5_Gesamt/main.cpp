
#include "glut_handling.h"

#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
	GlutHandling gh;
	if (!gh.init(argc, argv)) {
		cerr << "Init Error" << endl;
	}
	gh.run();
	return 0;
}
