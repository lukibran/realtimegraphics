
#ifndef __KD_NODE_H__
#define __KD_NODE_H__

class KdNode
{
public:
	enum Direction
	{
		DIR_NONE = 0, // for KdLeaf
		DIR_X,
		DIR_Y,
		DIR_Z,
	};
	KdNode(Direction dir, float border);
	virtual ~KdNode();
//protected:
	Direction mDir;
	float mBorder;
	KdNode* mLess; // < ... left side
	KdNode* mMore; // >= ... right side
};

#endif