
#include "point.h"
#include <glm/./core/type.hpp>

Point::Point(const glm::vec3& p)
		:mX(p.x), mY(p.y), mZ(p.z)
{
}

Point::Point(float x, float y, float z)
		:mX(x), mY(y), mZ(z)
{
}

Point::~Point()
{
}

Point operator+(const Point& p1, const Point& p2)
{
	return Point(p1.mX + p2.mX, p1.mY + p2.mY, p1.mZ + p2.mZ);
}

Point operator/(const Point& p, float a)
{
	return Point(p.mX / a, p.mY / a, p.mZ / a);
}

Point operator*(const Point& p, float a)
{
	return Point(p.mX * a, p.mY * a, p.mZ * a);
}

Point& Point::operator+=(const Point& p)
{
	mX += p.mX;
	mY += p.mY;
	mZ += p.mZ;
	return *this;
}

float Point::getValue(KdNode::Direction dir) const
{
	switch (dir) {
		case KdNode::DIR_X:
			return mX;
		case KdNode::DIR_Y:
			return mY;
		case KdNode::DIR_Z:
			return mZ;
		default:
			break;
	}
	return 0.0f;
}

glm::vec3 Point::getVec3() const
{
	return glm::vec3(mX, mY, mZ);
}

float Point::getLength() const
{
	return sqrtf(mX * mX + mY * mY + mZ * mZ);
}
