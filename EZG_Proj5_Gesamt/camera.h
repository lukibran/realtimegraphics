
#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <glm/gtc/quaternion.hpp>
#include <vector>

class Camera
{
public:
	/**
	 * @param rotation unit: degree
	 */
	Camera();
	virtual ~Camera();

	void update(double seconds);
	//void setSpeed(float speed);

	void moveForward(bool forward);
	void moveBackward(bool backward);

	void rotateLeft(bool left);
	void rotateRight(bool right);

	void rotateUp(bool up);
	void rotateDown(bool down);

	void rotateClockwise(bool clockwise);
	void rotateAntiClockwise(bool antiClockwise);

	float getViewAngle() const;
	float getZNear() const;
	float getZFar() const;

	/**
	 * @param eye OUT store eye x, y, z
	 * @param center OUT store center x, y, z
	 * @param up OUT store up x, y, z
	 */
	void getPropertiesForGluLookAt(glm::vec3& eye, glm::vec3& center,
			glm::vec3& up) const;
	glm::vec3 getPos() const;
	glm::vec3 getViewVector() const;
	//glm::mat4 getAxisAngleMatrix() const;

	/**
	 * @param v Must be a normalized vector
	 */
	static glm::vec3 getUpVector(const glm::vec3& v);
private:
	float mViewAngle;
	float mZNear;
	float mZFar;

	// eye position
	glm::vec3 mPosPoint;
	glm::vec3 mViewVector; // normalized
	glm::vec3 mUpVector; // normalized

	double mPrevSeconds;
	bool mMoveForward;
	bool mMoveBackward;

	bool mRotateLeft;
	bool mRotateRight;
	bool mRotateUp;
	bool mRotateDown;

	bool mRotateClockwise;
	bool mRotateAntiClockwise;

	// start time of the camera movment
	double mStartTime;
	// time between two points
	double mPointDiffTime;
	std::vector<glm::vec3> mPoints;
	std::vector<glm::vec3> mViewPoints;

	glm::vec3 getInterPoint(const std::vector<glm::vec3>& points,
			unsigned int index, double percent) const;

	glm::quat getInterQuatPoint(unsigned int index, double percent) const;

	void updateCustomPosition(double seconds);
};

#endif
