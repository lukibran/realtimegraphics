
#ifndef __POINT_H__
#define __POINT_H__

#include "kd_node.h"
#include <glm/gtc/quaternion.hpp>

class Point
{
public:
	float mX, mY, mZ;
	Point(const glm::vec3& p);
	Point(float x, float y, float z);
	virtual ~Point();
	friend Point operator+(const Point& p1, const Point& p2);
	friend Point operator/(const Point& p, float a);
	friend Point operator*(const Point& p, float a);
	Point& operator+=(const Point& p);
	float getValue(KdNode::Direction dir) const;
	glm::vec3 getVec3() const;
	float getLength() const;
};

#endif