
#ifndef __GLUT_HANDLING_H__
#define __GLUT_HANDLING_H__

class Graphic;

class GlutHandling
{
public:
	GlutHandling();
	virtual ~GlutHandling();

	bool init(int argc, char** argv);
	void run();
private:
	static GlutHandling* sGh;

	Graphic* mGraphic;

	static void visibilityFunc(int state);
	static void reshapeFunc(int width, int height);
	static void displayFunc();
	static void idleFunc();
	static void overlayDisplayFunc();
	static void closeFunc();
	static void exitFunc();

	static void keyboardPressFunc(unsigned char key, int x, int y);
	static void keyboardReleaseFunc(unsigned char key, int x, int y);
	static void specialKeyboardPressFunc(int key, int x, int y);
	static void specialKeyboardReleaseFunc(int key, int x, int y);
	static void keyboardFunc(int key, int x, int y, bool isPressed);

	static void mouseFunc(int button, int state, int x, int y);

	static void activeMotionFunc(int x, int y);
	static void passiveMotionFunc(int x, int y);
	static void motionFunc(int x, int y, bool isPassive);

	void visibility(int state);
	void reshape(int width, int height);
	void display();
	void idle();
	void overlayDisplay();
	void close();
	void exit();

	void keyboard(int key, int x, int y, bool isPressed);

	void mouse(int button, int state, int x, int y);

	void motion(int x, int y, bool isPassive);
};

#endif
