
#include "triangle.h"
#include <glm/./core/type.hpp>

Triangle::Triangle(const Point& a, const Point& b, const Point& c)
		:mA(a), mB(b), mC(c), mCenter((a + b + c) / 3)
{
}

/**
 * Ray/triangle intersection using the algorithm proposed by Möller and Trumbore (1997).
 *
 * Input:
 *    o : origin.
 *    d : direction.
 *    p0, p1, p2: vertices of the triangle.
 * Output:
 *    flag: (0) Reject, (1) Intersect.
 *    u,v: barycentric coordinates.
 *    t: distance from the ray origin.
 * Author:
 *    Jesus Mena
 */
bool Triangle::rayTriangleIntersection(const glm::vec3& o, const glm::vec3 d,
			float& u, float& v, float& t)
{
	float epsilon = 0.00001f;

	glm::vec3 e1 = mB.getVec3() - mA.getVec3();
	glm::vec3 e2 = mC.getVec3() - mA.getVec3();

	glm::vec3 q = glm::cross(d, e2);
	float a = glm::dot(e1, q); // determinant of the matrix M
	if (a > -epsilon && a < epsilon) {
		// the vector is parallel to the plane (the intersection is at infinity)
		return false;
	}

	float f = 1/a;
	glm::vec3 s = o - mA.getVec3();
	u = f * glm::dot(s, q);

	if (u < 0.0) {
		// the intersection is outside of the triangle
		return false;
	}

	glm::vec3 r = glm::cross(s, e1);
	v = f * glm::dot(d, r);

	if (v < 0.0 || u + v > 1.0) {
		// the intersection is outside of the triangle
		return false;
	}

	t = f * glm::dot(e2, r); // verified!
	return true;
}

Triangle operator+(const Triangle& t1, const Point& p)
{
	return Triangle(t1.mA + p, t1.mB + p, t1.mC + p);
}
