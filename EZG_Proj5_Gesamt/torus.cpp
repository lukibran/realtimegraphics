#include "torus.h"
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

#define WIN32_LEAN_AND_MEAN
#include <GL/glext.h>
#include "Extensions/ARB_multitexture_extension.h"
#include "Extensions/ARB_texture_cube_map_extension.h"
#include "Extensions/ARB_texture_env_combine_extension.h"
#include "Extensions/ARB_texture_env_dot3_extension.h"


#include "triangle.h"

static glm::vec3 getRotatedX(const glm::vec3& v, double angle)
{
	if(angle==0.0)
		return v;

	float sinAngle=(float)sin(M_PI*angle/180);
	float cosAngle=(float)cos(M_PI*angle/180);

	return glm::vec3(	v.x,
						v.y*cosAngle - v.z*sinAngle,
						v.y*sinAngle + v.z*cosAngle);
}

static glm::vec3 getRotatedY(const glm::vec3& v, double angle)
{
	if(angle==0.0)
		return v;

	float sinAngle=(float)sin(M_PI*angle/180);
	float cosAngle=(float)cos(M_PI*angle/180);

	return glm::vec3(	v.x*cosAngle + v.z*sinAngle,
						v.y,
						-v.x*sinAngle + v.z*cosAngle);
}

static glm::vec3 getRotatedZ(const glm::vec3& v, double angle)
{
	if(angle==0.0)
		return v;

	float sinAngle=(float)sin(M_PI*angle/180);
	float cosAngle=(float)cos(M_PI*angle/180);

	return glm::vec3(v.x*cosAngle - v.y*sinAngle,
					v.x*sinAngle + v.y*cosAngle,
					v.z);
}

Torus::Torus(float xOffset, float yOffset, float zOffset, float r1, float r2, int precision,
		std::vector<Triangle>* triangles)
		:mXOffset(xOffset), mYOffset(yOffset), mZOffset(zOffset),
		mR1(r1), mR2(r2), torusPrecision(precision), mTriangles(triangles)

{
	initTorus();
}

Torus::~Torus()
{
	if(indices)
		delete [] indices;
	indices=NULL;

	if(vertices)
		delete [] vertices;
	vertices=NULL;
}

bool Torus::initTorus()
{
	numVertices=(torusPrecision+1)*(torusPrecision+1);
	numIndices=2*torusPrecision*torusPrecision*3;

	vertices=new TorusVertex[numVertices];
	if(!vertices)
	{
		printf("Unable to allocate memory for torus vertices\n");
		return false;
	}

	indices=new unsigned int[numIndices];
	if(!indices)
	{
		printf("Unable to allocate memory for torus indices\n");
		return false;
	}
	
	//calculate the first ring - inner radius 4, outer radius 1.5
	for(int i=0; i<torusPrecision+1; i++)
	{
		vertices[i].position = getRotatedZ(glm::vec3(mR1, 0.0f, 0.0f),
				i*360.0f/torusPrecision) + glm::vec3(mR2, 0.0f, 0.0f);
		
		vertices[i].s=0.0f;
		vertices[i].t=(float)i/torusPrecision;

		vertices[i].sTangent = glm::vec3(0.0f, 0.0f, -1.0f);
		vertices[i].tTangent= getRotatedZ(
				glm::vec3(0.0f, -1.0f, 0.0f), i*360.0f/torusPrecision);
		vertices[i].normal = glm::cross(vertices[i].tTangent,
				vertices[i].sTangent);
	}

	//rotate this to get other rings
	for(int ring=1; ring<torusPrecision+1; ring++)
	{
		for(int i=0; i<torusPrecision+1; i++)
		{
			vertices[ring*(torusPrecision+1)+i].position =
					getRotatedY(vertices[i].position, ring*360.0f/torusPrecision);
			
			vertices[ring*(torusPrecision+1)+i].s=2.0f*ring/torusPrecision;
			vertices[ring*(torusPrecision+1)+i].t=vertices[i].t;

			vertices[ring*(torusPrecision+1)+i].sTangent =
					getRotatedY(vertices[i].sTangent,
					ring*360.0f/torusPrecision);
			vertices[ring*(torusPrecision+1)+i].tTangent =
					getRotatedY(vertices[i].tTangent,
					ring*360.0f/torusPrecision);
			vertices[ring*(torusPrecision+1)+i].normal =
					getRotatedY(vertices[i].normal,
					ring*360.0f/torusPrecision);
		}
	}

	glm::vec3 posOffset(mXOffset, mYOffset, mZOffset);
	for(int ring=0; ring<torusPrecision+1; ring++)
	{
		for(int i=0; i<torusPrecision+1; i++)
		{
			vertices[ring*(torusPrecision+1)+i].position += posOffset;
		}
	}

	//calculate the indices
	for(int ring=0; ring<torusPrecision; ring++)
	{
		for(int i=0; i<torusPrecision; i++)
		{
			int index1 = ((ring*torusPrecision+i)*2)*3;
			int index2 = ((ring*torusPrecision+i)*2+1)*3;
			indices[index1 + 0] = ring*(torusPrecision+1)+i;
			indices[index1 + 1] = (ring+1)*(torusPrecision+1)+i;
			indices[index1 + 2] = ring*(torusPrecision+1)+i+1;

			indices[index2 + 0] = ring*(torusPrecision+1)+i+1;
			indices[index2 + 1] = (ring+1)*(torusPrecision+1)+i;
			indices[index2 + 2] = (ring+1)*(torusPrecision+1)+i+1;

			Triangle t1(
					Point(vertices[indices[index1 + 0]].position.x,
					vertices[indices[index1 + 0]].position.y,
					vertices[indices[index1 + 0]].position.z),
					Point(vertices[indices[index1 + 1]].position.x,
					vertices[indices[index1 + 1]].position.y,
					vertices[indices[index1 + 1]].position.z),
					Point(vertices[indices[index1 + 2]].position.x,
					vertices[indices[index1 + 2]].position.y,
					vertices[indices[index1 + 2]].position.z));

			Triangle t2(
					Point(vertices[indices[index2 + 0]].position.x,
					vertices[indices[index2 + 0]].position.y,
					vertices[indices[index2 + 0]].position.z),
					Point(vertices[indices[index2 + 1]].position.x,
					vertices[indices[index2 + 1]].position.y,
					vertices[indices[index2 + 1]].position.z),
					Point(vertices[indices[index2 + 2]].position.x,
					vertices[indices[index2 + 2]].position.y,
					vertices[indices[index2 + 2]].position.z));
			mTriangles->push_back(t1);
			mTriangles->push_back(t2);
		}
	}

	return true;
}

void Torus::calcTanSpaceLightVector(const glm::vec3& objectLightPosition)
{
	//Loop through vertices
	for(int i=0; i<numVertices; ++i)
	{
		glm::vec3 lightVector=objectLightPosition-vertices[i].position; // original

		lightVector = glm::normalize(glm::normalize(vertices[i].normal) +
				glm::normalize(lightVector));

		//Calculate tangent space light vector
		vertices[i].tangentSpaceLight.x =
				glm::dot(vertices[i].sTangent, lightVector);
		vertices[i].tangentSpaceLight.y =
			glm::dot(vertices[i].tTangent, lightVector);
		vertices[i].tangentSpaceLight.z =
			glm::dot(vertices[i].normal, lightVector);
	}
}

void Torus::drawBumpPass(GLuint normalMap, GLuint normalisationTorusMap)
{
		//Bind normal map to texture unit 0
		glBindTexture(GL_TEXTURE_2D, normalMap);
		glEnable(GL_TEXTURE_2D);

		//Bind normalisation cube map to texture unit 1
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, normalisationTorusMap);
		glEnable(GL_TEXTURE_CUBE_MAP_ARB);
		glActiveTextureARB(GL_TEXTURE0_ARB);

		//Set vertex arrays for torus
		glVertexPointer(3, GL_FLOAT, sizeof(TorusVertex), &vertices[0].position);
		glEnableClientState(GL_VERTEX_ARRAY);

		//Send texture coords for normal map to unit 0
		glTexCoordPointer(2, GL_FLOAT, sizeof(TorusVertex), &vertices[0].s);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		//Send tangent space light vectors for normalisation to unit 1
		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glTexCoordPointer(3, GL_FLOAT, sizeof(TorusVertex), &vertices[0].tangentSpaceLight);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);


		//Set up texture environment to do (tex0 dot tex1)*color
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_REPLACE);

		glActiveTextureARB(GL_TEXTURE1_ARB);

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_DOT3_RGB_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, GL_PREVIOUS_ARB);

		glActiveTextureARB(GL_TEXTURE0_ARB);



		//Draw torus
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices);


		//Disable textures
		glDisable(GL_TEXTURE_2D);

		glActiveTextureARB(GL_TEXTURE1_ARB);
		glDisable(GL_TEXTURE_CUBE_MAP_ARB);
		glActiveTextureARB(GL_TEXTURE0_ARB);

		//disable vertex arrays
		glDisableClientState(GL_VERTEX_ARRAY);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);

		//Return to standard modulate texenv
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

}

void Torus::drawColor(GLuint decalTexture, bool useTexture)
{
	if (useTexture) {
		//Bind decal texture
		glBindTexture(GL_TEXTURE_2D, decalTexture);
		glEnable(GL_TEXTURE_2D);
	}

	//Set vertex arrays for torus
	glVertexPointer(3, GL_FLOAT, sizeof(TorusVertex), &vertices[0].position);
	glEnableClientState(GL_VERTEX_ARRAY);

	glNormalPointer(GL_FLOAT, sizeof(TorusVertex), &vertices[0].normal);
	glEnableClientState(GL_NORMAL_ARRAY);

	glTexCoordPointer(2, GL_FLOAT, sizeof(TorusVertex), &vertices[0].s);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	//Draw torus
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices);
}
