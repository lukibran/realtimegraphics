#include "cube.h"
#include <stdio.h>
#include <math.h>
#include <glm/gtc/quaternion.hpp>
#include "point.h"
#include "triangle.h"

#define WIN32_LEAN_AND_MEAN
#include <GL/glext.h>
#include "Extensions/ARB_multitexture_extension.h"
#include "Extensions/ARB_texture_cube_map_extension.h"
#include "Extensions/ARB_texture_env_combine_extension.h"
#include "Extensions/ARB_texture_env_dot3_extension.h"

#define VERTEX_COUNT 24 //4x6
#define INDEX_COUNT 36 //6x6

Cube::Cube(float xOffset, float yOffset, float zOffset, float x, float y, float z,
		std::vector<Triangle>* triangles)
		:mXOffset(xOffset), mYOffset(yOffset), mZOffset(zOffset), mX(x), mY(y), mZ(z), mTriangles(triangles)
{
	initCube();
}

Cube::~Cube()
{
	if(indices)
		delete [] indices;
	indices=NULL;

	if(vertices)
		delete [] vertices;
	vertices=NULL;
}

bool Cube::initCube()
{
	numVertices = VERTEX_COUNT;
	vertices=new CubeVertex[numVertices];

	numIndices = INDEX_COUNT;
	indices=new unsigned int[numIndices];

	const unsigned int vOffset = 4;
	const unsigned int iOffset = 6;

	// Top face (y = 1.0f)
	unsigned int v = 0;
	unsigned int i = 0;
	initCubeFace(v, i,
		glm::vec3( 1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f, -1.0f),
		glm::vec3(-1.0f, 1.0f,  1.0f),
		glm::vec3( 1.0f, 1.0f,  1.0f));

	// Bottom face (y = -1.0f)
	v += vOffset;
	i += iOffset;
	initCubeFace(v, i,
		glm::vec3( 1.0f, -1.0f,  1.0f),
		glm::vec3(-1.0f, -1.0f,  1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3( 1.0f, -1.0f, -1.0f));

	// Front face  (z = 1.0f)
	v += vOffset;
	i += iOffset;
	initCubeFace(v, i,
		glm::vec3( 1.0f,  1.0f, 1.0f),
		glm::vec3(-1.0f,  1.0f, 1.0f),
		glm::vec3(-1.0f, -1.0f, 1.0f),
		glm::vec3( 1.0f, -1.0f, 1.0f));

	// Back face (z = -1.0f)
	v += vOffset;
	i += iOffset;
	initCubeFace(v, i,
		glm::vec3(-1.0f,  1.0f, -1.0f),
		glm::vec3( 1.0f,  1.0f, -1.0f),
		glm::vec3( 1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f));

	// Left face (x = -1.0f)
	v += vOffset;
	i += iOffset;
	initCubeFace(v, i,
		glm::vec3(-1.0f,  1.0f,  1.0f),
		glm::vec3(-1.0f,  1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f, -1.0f),
		glm::vec3(-1.0f, -1.0f,  1.0f));

	// Right face (x = 1.0f)
	v += vOffset;
	i += iOffset;
	initCubeFace(v, i,
		glm::vec3(1.0f,  1.0f, -1.0f),
		glm::vec3(1.0f,  1.0f,  1.0f),
		glm::vec3(1.0f, -1.0f,  1.0f),
		glm::vec3(1.0f, -1.0f, -1.0f));

	glm::vec3 scaleFactor(mX, mY, mZ);
	glm::vec3 posOffset(mXOffset, mYOffset, mZOffset);
	for (int ii = 0; ii < numVertices; ii++) {
		vertices[ii].position *= scaleFactor;
		vertices[ii].position += posOffset;
	}
	for (int ii = 0; ii < numIndices; ii += 3) {
		mTriangles->push_back(Triangle(
				Point(vertices[indices[ii + 0]].position),
				Point(vertices[indices[ii + 1]].position),
				Point(vertices[indices[ii + 2]].position)));
	}

	return true;
}

void Cube::calcTanSpaceLightVector(const glm::vec3& objectLightPosition)
{
	//Loop through vertices
	for(int i=0; i<numVertices; ++i)
	{
		glm::vec3 lightVector=objectLightPosition-vertices[i].position;

		lightVector = glm::normalize(glm::normalize(vertices[i].normal) +
				glm::normalize(lightVector));

		//Calculate tangent space light vector
		vertices[i].tangentSpaceLight.x =
				glm::dot(vertices[i].sTangent, lightVector);
		vertices[i].tangentSpaceLight.y =
				glm::dot(vertices[i].tTangent, lightVector);
		vertices[i].tangentSpaceLight.z =
				glm::dot(vertices[i].normal, lightVector);
	}
}

void Cube::drawBumpPass(GLuint normalMap, GLuint normalisationCubeMap)
{
		//Bind normal map to texture unit 0
		glBindTexture(GL_TEXTURE_2D, normalMap);
		glEnable(GL_TEXTURE_2D);

		//Bind normalisation cube map to texture unit 1
		glActiveTextureARB(GL_TEXTURE1_ARB);
		glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, normalisationCubeMap);
		glEnable(GL_TEXTURE_CUBE_MAP_ARB);
		glActiveTextureARB(GL_TEXTURE0_ARB);

		//Set vertex arrays for torus
		glVertexPointer(3, GL_FLOAT, sizeof(CubeVertex), &vertices[0].position);
		glEnableClientState(GL_VERTEX_ARRAY);

		//Send texture coords for normal map to unit 0
		glTexCoordPointer(2, GL_FLOAT, sizeof(CubeVertex), &vertices[0].s);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		//Send tangent space light vectors for normalisation to unit 1
		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glTexCoordPointer(3, GL_FLOAT, sizeof(CubeVertex), &vertices[0].tangentSpaceLight);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);


		//Set up texture environment to do (tex0 dot tex1)*color
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_REPLACE);

		glActiveTextureARB(GL_TEXTURE1_ARB);

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
		glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_DOT3_RGB_ARB);
		glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, GL_PREVIOUS_ARB);

		glActiveTextureARB(GL_TEXTURE0_ARB);



		//Draw torus
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices);


		//Disable textures
		glDisable(GL_TEXTURE_2D);

		glActiveTextureARB(GL_TEXTURE1_ARB);
		glDisable(GL_TEXTURE_CUBE_MAP_ARB);
		glActiveTextureARB(GL_TEXTURE0_ARB);

		//disable vertex arrays
		glDisableClientState(GL_VERTEX_ARRAY);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glClientActiveTextureARB(GL_TEXTURE1_ARB);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glClientActiveTextureARB(GL_TEXTURE0_ARB);

		//Return to standard modulate texenv
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}

void Cube::drawColor(GLuint decalTexture, bool useTexture)
{
	if (useTexture) {
		//Bind decal texture
		glBindTexture(GL_TEXTURE_2D, decalTexture);
		glEnable(GL_TEXTURE_2D);
	}

	//Set vertex arrays for torus
	glVertexPointer(3, GL_FLOAT, sizeof(CubeVertex), &vertices[0].position);
	glEnableClientState(GL_VERTEX_ARRAY);

	glNormalPointer(GL_FLOAT, sizeof(CubeVertex), &vertices[0].normal);
	glEnableClientState(GL_NORMAL_ARRAY);

	glTexCoordPointer(2, GL_FLOAT, sizeof(CubeVertex), &vertices[0].s);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices);
}

void Cube::drawSimpleColor(float* color)
{
	glColor4f(color[0], color[1], color[2], color[3]);
	//glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indices);
	
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < numIndices; i += 3) {
		glm::vec3& p1 = vertices[indices[i + 0]].position;
		glm::vec3& p2 = vertices[indices[i + 1]].position;
		glm::vec3& p3 = vertices[indices[i + 2]].position;
		glVertex3f(p1.x, p1.y, p1.z);
		glVertex3f(p2.x, p2.y, p2.z);
		glVertex3f(p3.x, p3.y, p3.z);
	}
	glEnd();
}

void Cube::initCubeFace(unsigned int vOffset, unsigned int iOffset,
		const glm::vec3& v1,
		const glm::vec3& v2,
		const glm::vec3& v3,
		const glm::vec3& v4)
{
	vertices[vOffset + 0].position = v1;
	vertices[vOffset + 0].s = 1.0f;
	vertices[vOffset + 0].t = 1.0f;
	vertices[vOffset + 1].position = v2;
	vertices[vOffset + 1].s = 0.0f;
	vertices[vOffset + 1].t = 1.0f;
	vertices[vOffset + 2].position = v3;
	vertices[vOffset + 2].s = 0.0f;
	vertices[vOffset + 2].t = 0.0f;
	vertices[vOffset + 3].position = v4;
	vertices[vOffset + 3].s = 1.0f;
	vertices[vOffset + 3].t = 0.0f;
	glm::vec3 sTangent = glm::normalize(vertices[vOffset + 1].position -
			vertices[vOffset + 0].position);
	glm::vec3 tTangent = glm::normalize(vertices[vOffset + 1].position -
			vertices[vOffset + 2].position);
	glm::vec3 normal = glm::cross(tTangent, sTangent);
	for (int i = 0; i < 4; i++) {
		vertices[vOffset + i].sTangent = sTangent;
		vertices[vOffset + i].tTangent = tTangent;
		vertices[vOffset + i].normal = normal;
	}
	indices[iOffset + 0] = vOffset + 0;
	indices[iOffset + 1] = vOffset + 1;
	indices[iOffset + 2] = vOffset + 2;
	indices[iOffset + 3] = vOffset + 0;
	indices[iOffset + 4] = vOffset + 2;
	indices[iOffset + 5] = vOffset + 3;
}
