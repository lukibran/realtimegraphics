#ifndef CUBE_H
#define CUBE_H

#include <Windows.h>
#include <glm/gtc/quaternion.hpp>
#include <GL/gl.h>
#include <vector>

class CubeVertex
{
public:
	glm::vec3 position;
	float s, t;
	glm::vec3 sTangent, tTangent;
	glm::vec3 normal;
	glm::vec3 tangentSpaceLight;
};

class Triangle;

class Cube
{
public:
	Cube(float xOffset, float yOffset, float zOffset, float x, float y, float z,
			std::vector<Triangle>* triangles);
	~Cube();

	bool initCube();

	int numVertices;
	int numIndices;

	unsigned int * indices;
	CubeVertex * vertices;

	//Calculate tangent space light vector
	void calcTanSpaceLightVector(const glm::vec3& objectLightPosition);

	//Draw bump pass
	void drawBumpPass(GLuint normalMap, GLuint normalisationCubeMap);

	//Draw color
	//void drawColor(GLuint decalTexture);
	void drawColor(GLuint decalTexture, bool useTexture);
	void drawSimpleColor(float* color);

	const float mXOffset, mYOffset, mZOffset;
	const float mX, mY, mZ;
	std::vector<Triangle>* mTriangles;

	void initCubeFace(unsigned int vOffset, unsigned int iOffset,
			const glm::vec3& v1,
			const glm::vec3& v2,
			const glm::vec3& v3,
			const glm::vec3& v4);
};

#endif	//TORUS_H
