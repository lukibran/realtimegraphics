
#ifndef __GRAPHIC_H__
#define __GRAPHIC_H__

#include "camera.h"

#include <Windows.h>
#include <string>
#include <stdint.h>
#include <GL/gl.h>
#include "torus.h"

#include "triangle.h"
#include <vector>
#include "kd_node.h"
#include "cube.h"

#define CUBE_COUNT 6

//class Cube;
//class KdNode;
class Point;
class Vector;

class Graphic
{
public:
	Graphic(unsigned int width, unsigned int height);
	virtual ~Graphic();

	bool init();

	void reshape(unsigned int width, unsigned int height);

	void update();

	Camera* getCamera() { return &mCam; }
	void renderdrawBump();
	void renderwireframe();
	void renderKdTree();
	void renderdrawMode();
	void renderAntialiasing();

	void delPoints();
	void setPoint();
private:
	unsigned int w, h;

	int64_t mStartTime;

	Camera mCam;

	// Size of shadow map
	const int mShadowMapSize;

	// Textures
	GLuint mShadowMapTexture;

	// Matrices
	glm::mat4 mLightProjectionMatrix, mLightViewMatrix;
	glm::mat4 mCameraProjectionMatrix, mCameraViewMatrix;
	glm::vec3 mLightPos;
	glm::vec3 mLightCenter;
	glm::vec3 mLightUp;

	std::vector<Triangle> mTriangles;

	//Normal map
	GLuint mNormalMap;
	//Decal texture
	GLuint mDecalTexture;
	//Normalisation cube map
	GLuint mNormalisationCubeMap;

	Torus mTorus;
	Torus mTorus2;
	bool mDrawBumps;
	bool mDrawColor;
	bool mWireframe;
	bool mDrawKdTree;
	unsigned int mDrawMode; // 0 = normal with color and tex, 1 = triangles, 2 = none
	bool mAntialiasing;

	KdNode* mKdRoot;

	bool mReticleIsSet;
	glm::vec3 mReticle;

	bool point1Set;
	glm::vec3 point1;

	bool point2Set;
	glm::vec3 point2;

	Cube mFloor;
	Cube *mCube[CUBE_COUNT];
	GLuint mCubeNormalMap[CUBE_COUNT];
	GLuint mCubeDecalTexture[CUBE_COUNT];

	void enableDisableCullface();
	void drawForPass(unsigned int passNr);
	void drawTorus(bool drawBumps, bool drawColor, bool setColor);
	void printInfo();
	int64_t getTime() const;

	void setCamera();

	static glm::vec4 getRowFromMat4(const glm::mat4& m, unsigned int rowIndex);

	void loadMaps();
	static void loadMaps(GLuint& normalMap, GLuint& decalTexture, const std::string& normalFile, const std::string& decalFile);
	void calcTanSpaceLightVectorForCubes(const glm::vec3& objectLightPosition);
	void drawCubes(bool drawBumps, bool drawColor, bool setColor);
	void drawCube(bool drawBumps, bool drawColor, bool setColor, Cube& cube, unsigned int index);
	void finalDrawCubes();
	
	// Methoden f�r KD-Tree
	void buildKdTree();
	static bool buildKdNode(KdNode** parrent, const std::vector<Triangle*>& triangles);
	static bool kdSplit(KdNode** parrent, const std::vector<Triangle*>& triangles, std::vector<float>& v, KdNode::Direction dir);
	static bool insertTrianglesIntoKdNode(KdNode* n, const std::vector<Triangle*>& triangles);
	void drawKdTree();
	void drawTriangles();
	static void drawKdTree(const Point& minP, const Point& maxP, KdNode* n);
	static Triangle* visitNodes(KdNode *n, const Point& a, const Vector& d, float tmax, float& distance);

};

#endif
