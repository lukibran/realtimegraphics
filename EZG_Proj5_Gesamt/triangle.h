
#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "point.h"
#include <glm/gtc/quaternion.hpp>

class Triangle
{
public:
	Point mA, mB, mC, mCenter;
	Triangle(const Point& a, const Point& b, const Point& c);

	/**
	 * @return true for intersection
	 */
	bool rayTriangleIntersection(const glm::vec3& o, const glm::vec3 d,
			float& u, float& v, float& t);

	friend Triangle operator+(const Triangle& t1, const Point& p);
};

#endif