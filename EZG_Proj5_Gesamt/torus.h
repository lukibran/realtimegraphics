#ifndef TORUS_H
#define TORUS_H

#include <Windows.h>
#include <GL/gl.h>
#include <glm/gtc/quaternion.hpp>

#include <vector>

class Triangle;

class TorusVertex
{
public:
	glm::vec3 position;
	float s, t;
	glm::vec3 sTangent, tTangent;
	glm::vec3 normal;
	glm::vec3 tangentSpaceLight;
};

class Torus
{
public:
	Torus(float xOffset, float yOffset, float zOffset, float r1, float r2, int precision,
			std::vector<Triangle>* triangles);
	~Torus();

	bool initTorus();

	int numVertices;
	int numIndices;

	unsigned int * indices;
	TorusVertex * vertices;

	//Calculate tangent space light vector
	void calcTanSpaceLightVector(const glm::vec3& objectLightPosition);

	//Draw bump pass
	void drawBumpPass(GLuint normalMap, GLuint normalisationTorusMap);

	//Draw color
	//void drawColor(GLuint decalTexture);
	void drawColor(GLuint decalTexture, bool useTexture);

	const float mXOffset, mYOffset, mZOffset;
	const float mR1, mR2;
	const int torusPrecision;
	std::vector<Triangle>* mTriangles;
};

#endif	//TORUS_H
